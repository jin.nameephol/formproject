import { Component,OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { PeopleService } from '../people.service'
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {
  profileForm = new FormGroup({
    id: new FormControl(''),
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    address: new FormControl(''),
  });


  name: string;
  arrList: any = [];


  constructor(private peopleService: PeopleService,
    private httpClient: HttpClient) {
    console.log(this.peopleService);

  }

  ngOnInit() {
  this.getData();
  }

  getData(){
    this.httpClient.get('http://localhost:3000/users').toPromise().then((res) => {
      this.arrList = res;
      console.log(this.arrList);

    })
  }

  onclick() {
    this.httpClient.post<any>('http://localhost:3000/newuser', this.profileForm.value)
      .toPromise()
      .then((res) => {
        id : res.id
        firstname  : res.firstname
        lastname : res.lastname
        address : res.address
          console.log(res);
      }).then(() =>{
        this.getData()
      })
  }

  onDelete(){
    this.httpClient.delete<any>('/users/{id}')
  }
}
